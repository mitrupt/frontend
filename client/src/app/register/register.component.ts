import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 
  name = '';
  email = '';
  gender = '';
  password = '';
  phone = '';
  role = '';
   

  constructor(
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
  }

  onSignup() {

    console.log(this.name)
    this.userService
      .signup(this.name, this.email, this.password,this.role)
      .subscribe(response => {
        const result = response.json();
        console.log(result.data);
        if (result.status === 'error') {
          alert('Registration Failed');
        } else {
          alert('Registration Successful');
          this.router.navigate(['/login']);
        }
      });
  }

}