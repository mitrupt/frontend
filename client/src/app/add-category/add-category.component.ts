import { Component, OnInit } from '@angular/core';
import { categoryService} from '../category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
     category_name = '';
     thumbnail = '';

  constructor(
    private router: Router,
    private categoryService: categoryService) { }
  
  ngOnInit() {
  }
onAdd() {
  this.categoryService
   .addCategory(this.category_name,this.thumbnail)
   .subscribe(response => {
    console.log(response);

    this.router.navigate(['/category-list']);
   });     
}
  onCancel(){
    this.router.navigate(['/category-list']);
  }
}
