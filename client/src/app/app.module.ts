import { RegisterComponent } from './register/register.component';
import { categoryService } from './category.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HelpComponent } from './help/help.component';

import { LoginComponent } from './login/login.component';
import { UserService } from './user.service';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryListComponent } from './category-list/category-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AddCategoryComponent,
    CategoryListComponent,
    AboutComponent,
    HelpComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'category-add', component: AddCategoryComponent, canActivate: [UserService] },
      { path: 'category-list', component: CategoryListComponent },
      { path: 'about', component: AboutComponent },
      { path: 'help', component: HelpComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component:RegisterComponent}
    ])
  ],
  providers: [
    UserService,
    categoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
