import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class categoryService {

  url = 'http://localhost:3000/category';

  constructor(private http: Http) {

  }

  getCategories() {
    return this.http.get(this.url);
  }

  getCategoryDetails(id: number) {
    return this.http.get(this.url + '/' + id);
  }

  addCategory( category_name: String,
    thumbnail: String) {

      const body = {
        category_name: category_name,
        thumbnail: thumbnail
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });

      return this.http.post(this.url, body, requestOption);
  }

  updateCategory() {

  }

  deleteCategory(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}
