import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email_id = '';
  password = '';
  constructor(
     private router: Router,
    private userService: UserService) { }

  ngOnInit() {
  }

  onCancel() {
    this.router.navigate(['/category-list']);
  }

  onLogin() {

    this.userService
      .signin(this.email_id, this.password)
      .subscribe(response => {
        const result = response.json();
        console.log(result.data);
        if (result.status === 'error') {
          alert('invalid email or password');
        } else {
          if(result.data.role == "admin"){

            sessionStorage['login_status'] = '1';
            sessionStorage['user_name'] = result.data.user_name;
  
  
            alert('welcome to the application');
            this.router.navigate(['/category-add']);

          }else{

            this.router.navigate(['/userhomepage']);


          }
         
        }
      });
  }

  onSignup() {
    this.router.navigate(['/register']);
  }
}