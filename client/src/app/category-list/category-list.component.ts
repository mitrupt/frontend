import { Component, OnInit } from '@angular/core';
import { categoryService } from '../category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

categories = [];

  // dependancy injection
  constructor(
    private router: Router,
    private categoryService: categoryService) {

    this.refreshCategoryList();
  }

  refreshCategoryList() {
    this.categoryService
      .getCategories()
      .subscribe(response => {
        const result = response.json();
        console.log(result);
        this.categories = result.data;
      });
  }

  ngOnInit() {
  }

  onDetails(category) {
    // alert('showing the details of : ' + movie.title);
    this.router.navigate(['/category-details'], { queryParams: { id: category.category_id } });
  }

  onDelete(category) {
    const answer = confirm('Are you sure you want to delete ' +  category.category_name + ' ?');
    if (answer) {
      this.categoryService
        .deleteCategory(category.category_id)
        .subscribe(response => {
          const result = response.json();
          console.log(result);
          this.refreshCategoryList();
        });
    }
  }

}
